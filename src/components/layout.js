import React from 'react'
import { StaticQuery } from 'gatsby'
import { graphql } from 'gatsby'
import { Helmet } from 'react-helmet'

// Import styles + fonts
import '../scss/main.scss'
import 'typeface-open-sans'
import 'typeface-arimo'

// Import Components
import Header from '../components/header'
import Footer from '../components/footer'
// Import images used
import HomeBGImage from '../assets/svg/slider-bg-1.svg'
import PageBGImage from '../assets/images/banner-bg.png'

const Layout = ({ children, location }) => (
  <StaticQuery
    query={graphql`
      query LayoutQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => {
      if (location.pathname === '/') {
        return (
          <div
            className="page"
            style={{
              background: `url(${HomeBGImage}) no-repeat scroll right top`,
            }}
          >
            <Helmet htmlAttributes={{ lang: 'en' }}>
              <meta charSet="utf-8" />
              <title>Cirrus Creative</title>
              <meta
                name="description"
                content="Creating online experiences. Start your journey to an interactive and engaging site."
              />
            </Helmet>
            <Header />
            {children}
            <Footer />
          </div>
        )
      } else {
        return (
          <div
            className="page"
            style={{
              background: `url(${PageBGImage}) no-repeat scroll right top`,
            }}
          >
            <Helmet htmlAttributes={{ lang: 'en' }}>
              <meta charSet="utf-8" />
              <title>Cirrus Creative</title>
              <meta
                name="description"
                content="Creating online experiences. Start your journey to an interactive and engaging site."
              />
            </Helmet>
            <Header />
            {children}
            <Footer />
          </div>
        )
      }
    }}
  />
)

export default Layout
