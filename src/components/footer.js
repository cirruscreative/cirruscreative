import React from 'react'

import SignupForm from './signupForm'

// Import BG Image
import BGImage from '../assets/images/footer-bg.jpg'

const Footer = () => (
  <footer
    className="footer"
    style={{ background: `url(${BGImage}) no-repeat scroll center top` }}
  >
    <div className="footer-column">
      <p>
        Cirrus Creative © 2018
        <br />
        <br />
        hello@cirruscreative.co.uk
      </p>
    </div>
    <div className="footer-column">
      <p>Subscribe to new posts</p>
      <SignupForm />
    </div>
  </footer>
)

export default Footer
