import React from 'react'
import { Link } from 'gatsby'

import Logo from '../assets/logos/cirruscreative.svg'

// <Link className="nav-item" to={'/blog'} activeClassName={'is-active'}>
//   blog
// </Link>

export default class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isActive: false }
    this.changeActive = this.changeActive.bind(this)
  }

  changeActive() {
    const isActive = !this.state.isActive
    this.setState({ isActive })
  }

  render() {
    return (
      <div className="navbar">
        <Link className="navbar-logo" to={'/'}>
          <img
            className="logo"
            src={Logo}
            alt="Cirrus Creative Logo"
            width="300px"
          />
        </Link>
        <div role="button" className="navbar-menu" onClick={this.changeActive} onKeyUp={this.changeActive}>
          Menu
        </div>
        <nav
          className={
            this.state.isActive
              ? 'navbar-items navbar-items-show'
              : 'navbar-items'
          }
        >
          <Link className="nav-item is-secondary-nav" to={'/'}>
            home
          </Link>
          <Link className="nav-item is-secondary-nav" to="/#about">
            about
          </Link>
          <Link className="nav-item" to="/#services">
            services
          </Link>
          <Link className="nav-item" to="/#contact">
            contact
          </Link>
        </nav>
      </div>
    )
  }
}
