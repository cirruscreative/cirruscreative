import React from 'react'
import { request } from 'graphql-request'

const mutation = `mutation signup($email: String!) {
  signup(email: $email)
}`

export default class Contact extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      buttonText: 'Subscribe',
    }
  }

  handleChange = e => {
    this.setState({ email: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    this.setState({
      buttonText: 'Sending...',
    })
    request('https://graphql.cirruscreative.co.uk', mutation, this.state)
      .then(() => {
        this.setState({
          formCompleted: true,
          error: false,
          buttonText: 'Subscribe',
        })
      })
      .catch(err => {
        this.setState({
          error: err.response.errors[0].message,
          buttonText: 'Subscribe',
        })
      })
  }

  render() {
    return (
      <form name="signup" onSubmit={this.handleSubmit}>
        <noscript>
          <p>This form won’t work with Javascript disabled</p>
        </noscript>
        <label htmlFor="footer-email" style={{ display: 'none' }}>
          Enter Your Email Address
        </label>
        <input
          id="footer-email"
          name="email"
          type="email"
          className="input-footer-newsletter"
          placeholder="Enter your email address"
          onChange={this.handleChange}
          style={{
            outline: this.state.formCompleted ? `2px solid green` : `none`,
          }}
          aria-label="Enter your Email address to subscribe"
          required
        />{' '}
        <br />
        <small
          style={{
            display: this.state.formCompleted ? `block` : `none`,
            marginTop: '1rem',
          }}
        >
          Thanks for joining us! You will receive a confirmation email shortly.
        </small>
        <small
          style={{
            display: this.state.error ? `block` : `none`,
            marginTop: '1rem',
          }}
        >
          {this.state.error}
        </small>
        <button type="submit" className="button button-footer-newsletter">
          {this.state.buttonText}
        </button>
      </form>
    )
  }
}
