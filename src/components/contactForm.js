import React from 'react'
import { navigate } from 'gatsby'
import { request } from 'graphql-request'
import Recaptcha from 'react-google-recaptcha'

const RECAPTCHA_KEY = '6LfLF24UAAAAAHpvnnwv46zbHZRgLeH5ZkIfQsCV'

const mutation = `mutation contact($name: String!, $email: String!, $subject: String!, $message: String!) {
  contact(
    name: $name,
    email: $email,
    subject: $subject,
    message: $message
  )
}`

export default class Contact extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleMessageChange = e => {
    this.setState({ message: e.target.value, messageStatus: false })
  }

  handleRecaptcha = value => {
    this.setState({ recaptcha: value, recaptchaStatus: 'transparent' })
  }

  loadingButton = () => {
    const button = document.getElementById('loading-button')
    let currentStyle = ''
    setInterval(function(){
      if (currentStyle === '') {
        button.style.backgroundPosition = 'right center'
        currentStyle = 'right center'
      } else {
        button.style.backgroundPosition = ''
        currentStyle = ''
      }
    }, 400)
  }

  handleSubmit = e => {
    e.preventDefault()
    if (!this.state.recaptcha) {
      this.setState({ recaptchaStatus: 'red' })
    } else if (!this.state.message) {
      this.setState({ messageStatus: true })
    } else {
      this.loadingButton()
      const form = e.target
      request('https://graphql.cirruscreative.co.uk', mutation, this.state)
        .then(() => {
          navigate(form.getAttribute('action'))
        })
        .catch(err => console.error(err))
    }
  }

  render() {
    return (
      <form
        className="form"
        name="contact-recaptcha"
        method="post"
        action="/thanks/"
        onSubmit={this.handleSubmit}
      >
        <noscript>
          <p>This form won’t work with Javascript disabled</p>
        </noscript>
        <label htmlFor="contact-name" style={{ display: 'none' }}>
          Full Name
        </label>
        <input
          id="contact-name"
          className="input"
          type="text"
          name="name"
          placeholder="Name"
          onChange={this.handleChange}
          aria-label="Full Name"
          required
        />
        <label htmlFor="contact-email" style={{ display: 'none' }}>
          Email Address
        </label>
        <input
          id="contact-email"
          className="input"
          type="email"
          name="email"
          placeholder="Email"
          onChange={this.handleChange}
          aria-label="Email Address"
          required
        />
        <label htmlFor="contact-subject" style={{ display: 'none' }}>
          Message Subject
        </label>
        <input
          id="contact-subject"
          className="input subject"
          type="text"
          name="subject"
          placeholder="Subject"
          onChange={this.handleChange}
          aria-label="Message Subject"
          required
        />
        <label htmlFor="contact-message" style={{ display: 'none' }}>
          Message Text
        </label>
        <textarea
          id="contact-message"
          className="input message"
          name="message"
          placeholder="Message"
          onChange={this.handleMessageChange}
          style={{
            outline: this.state.messageStatus ? `2px solid red` : `none`,
          }}
          aria-label="Message text"
        />
        <Recaptcha
          className="recaptcha"
          style={{ backgroundColor: this.state.recaptchaStatus }}
          ref="recaptcha"
          sitekey={RECAPTCHA_KEY}
          onChange={this.handleRecaptcha}
        />
        <div className="submit">
          <button id="loading-button" className="button-footer-newsletter" type="submit">
            Send
          </button>
        </div>
      </form>
    )
  }
}
