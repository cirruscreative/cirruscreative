import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import ReactHtmlParser from 'react-html-parser'
import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
// Import Components
import Layout from '../components/layout'
import ContactForm from '../components/contactForm'
// Import Images
import HeroImage from '../assets/images/slider-m-1.png'
import TitleIcon from '../assets/images/icon/title-icon.png'
import TestimonialsBackground from '../assets/svg/3d-slider-shap.svg'
import DiscoverServicesImage from '../assets/images/iphone4.png'

// Dev
/* import readingTime from 'reading-time'
const timeToRead = (text) => {
  const stats = readingTime(text)
  return stats.text
} */

const IndexPage = ({
  data: {
    home: { data: home },
    testimonials,
  },
  location,
}) => (
  <Layout location={location}>
    <div className="home-page">
      <div className="hero">
        <div className="hero-text">
          <h1>Creating online experiences</h1>
          <h4>Start your journey to an interactive and engaging site</h4>
          <br />
          <a href="#services" className="button button-hero">
            Discover
          </a>
        </div>
        <div className="hero-image">
          <img src={HeroImage} alt="Computer" />
        </div>
      </div>
      <br />
      <div className="features-title">
        <img src={TitleIcon} alt="Features Icon" />
        <h5>{home.features_subtitle.text}</h5>
        <h1>{home.features_title.text}</h1>
      </div>
      <div className="features" id="features">
        {home.features.map((feature, index) => (
          <div className="feature-card" key={index}>
            <Img
              fixed={feature.icon.localFile.childImageSharp.fixed}
              alt={feature.title.text}
            />
            <h4 className="feature-title">{feature.title.text}</h4>
            <div className="feature-text">
              {ReactHtmlParser(feature.text.html)}
            </div>
            <a className="button-hero feature-button" href="#contact">
              Get in touch
            </a>
          </div>
        ))}
      </div>
      <div id="services" className="discover-services">
        <div className="testimonials">
          <img src={TestimonialsBackground} alt="Testimonials Background" />
          <div className="testimonial-carousel">
            <Carousel
              showArrows={false}
              showThumbs={false}
              showStatus={false}
              infiniteLoop={true}
              autoPlay={true}
            >
              {testimonials.edges.map(
                ({ node: { data: testimonial } }, index) => (
                  <div className="testimonial-slide" key={index}>
                    <div className="testimonial-content">
                      <h3>{testimonial.title.text}</h3>
                      {ReactHtmlParser(testimonial.text.html)}
                    </div>
                    <div className="testimonial-author">
                      <div className="testimonial-author-image">
                        <Img
                          className="testimonial-author-image"
                          fixed={
                            testimonial.author_image.localFile.childImageSharp
                              .fixed
                          }
                          alt={testimonial.author_name.text}
                        />
                      </div>
                      <p className="testimonial-author-name">
                        {testimonial.author_name.text}
                      </p>
                      <p className="testimonial-author-company">
                        {testimonial.author_company.text}
                      </p>
                    </div>
                  </div>
                )
              )}
            </Carousel>
          </div>
        </div>
        <div className="discover-services-content">
          <img src={TitleIcon} alt="Discover Features Icon" />
          <h5>{home.services_subtitle.text}</h5>
          <h2>{home.services_title.text}</h2>
          <div className="content">
            {ReactHtmlParser(home.services_text.html)}
          </div>
        </div>
        <div className="discover-services-image">
          <img src={DiscoverServicesImage} alt="Discover Services" />
        </div>
      </div>
      <div className="discover-us" id="about">
        <div className="discover-us-content">
          <img src={TitleIcon} alt="Discover Features Icon" />
          <h5>{home.us_subtitle.text}</h5>
          <h2>{home.us_title.text}</h2>
          <div className="content">{ReactHtmlParser(home.us_text.html)}</div>
          <div className="points">
            {home.us_points.map(({ point }, index) => (
              <p key={index}>{point.text}</p>
            ))}
          </div>
        </div>
        <div className="discover-us-gallery">
          {home.us_gallery.map(({ image }, index) => (
            <div key={index} className="discover-us-gallery-image">
              <Img fixed={image.localFile.childImageSharp.fixed} />
            </div>
          ))}
        </div>
      </div>
      <div className="contact" id="contact">
        <div className="contact-form">
          <ContactForm />
        </div>
        <div className="contact-content">
          <img src={TitleIcon} alt="Discover Features Icon" />
          <h5>{home.contact_subtitle.text}</h5>
          <h2>{home.contact_title.text}</h2>
          <div className="content">
            {ReactHtmlParser(home.contact_text.html)}
          </div>
          <h5>
            <strong>The obligatory social links:</strong>
          </h5>
          <div className="links">
            {home.contact_links.map((link, index) => (
              <a
                key={index}
                className="link-icon"
                href={link.url.raw.url}
                target={link.url.raw.target}
                rel="noopener"
              >
                <Img fixed={link.icon.localFile.childImageSharp.fixed} />
              </a>
            ))}
          </div>
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage

export const query = graphql`
  query IndexQuery {
    home: prismicHome {
      data {
        features_title {
          text
        }
        features_subtitle {
          text
        }
        features {
          title {
            text
          }
          text {
            html
          }
          button {
            text
          }
          icon {
            localFile {
              childImageSharp {
                fixed(height: 80) {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
          }
        }
        services_title {
          text
        }
        services_text {
          html
        }
        services_subtitle {
          text
        }
        services_image {
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        us_title {
          text
        }
        us_subtitle {
          text
        }
        us_text {
          html
        }
        us_points {
          point {
            text
          }
        }
        us_gallery {
          image {
            localFile {
              childImageSharp {
                fixed {
                  ...GatsbyImageSharpFixed_withWebp
                }
              }
            }
          }
        }
        contact_title {
          text
        }
        contact_subtitle {
          text
        }
        contact_text {
          html
        }
        contact_links {
          url {
            raw {
              target
              link_type
              url
            }
          }
          title {
            text
          }
          icon {
            localFile {
              childImageSharp {
                fixed(width: 24, height: 24) {
                  ...GatsbyImageSharpFixed_withWebp_tracedSVG
                }
              }
            }
          }
        }
      }
    }
    testimonials: allPrismicTestimonial(limit: 4) {
      edges {
        node {
          data {
            title {
              text
            }
            text {
              html
            }
            author_name {
              text
            }
            author_company {
              text
            }
            author_image {
              localFile {
                childImageSharp {
                  fixed(width: 80, height: 80, cropFocus: ENTROPY) {
                    ...GatsbyImageSharpFixed_withWebp
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
