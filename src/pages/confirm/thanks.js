import React from 'react'
import { Link } from 'gatsby'

import Layout from '../../components/layout'

const SecondPage = props => (
  <Layout location={props.location}>
    <div className="thanks-page">
      <h1>Thank you for subscribing.</h1>
      <p>Great content is on it's way to you now. Watch this space...</p>
      <small style={{ fontSize: '0.7rem' }}>
        Well, not this one. Your inbox might be a better space to watch.
      </small>
      <Link
        className="button-footer-newsletter"
        style={{ width: '7rem' }}
        to="/"
      >
        Go back home
      </Link>
    </div>
  </Layout>
)

export default SecondPage
