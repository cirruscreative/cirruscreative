import React from 'react'
import { Link, navigate } from 'gatsby'
import { request } from 'graphql-request'
// Import Components
import Layout from '../../components/layout'

function QueryStringToJSON(string) {
  if (!string) return null
  const pairs = string.slice(1).split('&')

  const result = {}
  pairs.forEach(function(pair) {
    pair = pair.split('=')
    result[pair[0]] = decodeURIComponent(pair[1] || '')
  })
  return JSON.parse(JSON.stringify(result))
}

const mutation = `mutation signupConfirm($lastname: String, $firstname: String, $code: String!) {
  signupConfirm(
    firstname: $firstname,
    lastname: $lastname,
    code: $code
  )
}`

class ConfirmForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      buttonText: 'Submit',
    }
  }

  async componentDidMount() {
    const params = await QueryStringToJSON(this.props.location.search)
    if (!params) navigate('/')
    else this.setState({ ...params })
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    this.setState({ buttonText: 'Loading..' })
    request('https://graphql.cirruscreative.co.uk', mutation, this.state)
      .then(() => navigate('/confirm/thanks'))
      .catch(({ response: { errors } }) => {
        const errorMessage = errors[0].message
        console.error(errorMessage)
        this.setState({
          buttonText: errorMessage,
        })
      })
  }

  render() {
    return (
      <form
        className="form confirm-form"
        name="contact-recaptcha"
        method="post"
        action="/confirm/thanks/"
        onSubmit={this.handleSubmit}
      >
        <noscript>
          <p>This form won’t work with Javascript disabled</p>
        </noscript>
        <input
          className="input"
          type="text"
          name="firstname"
          placeholder="First Name"
          onChange={this.handleChange}
          required
        />
        <input
          className="input"
          type="text"
          name="lastname"
          placeholder="Last Name"
          onChange={this.handleChange}
          required
        />
        <div className="submit">
          <button className="button-footer-newsletter" type="submit">
            {this.state.buttonText}
          </button>
        </div>
      </form>
    )
  }
}

const ConfirmPage = props => (
  <Layout location={props.location}>
    <div className="confirm-page">
      <h2>Nearly there...</h2>
      <p>
        Please fill in your first and last name, then click submit and all will
        be right with the world.
      </p>
      <ConfirmForm location={props.location} />
      <Link to="/">Go back to the homepage</Link>
    </div>
  </Layout>
)

export default ConfirmPage
