import React from 'react'
import { graphql } from 'gatsby'
import Image from 'gatsby-image'
import ReactHtmlParser from 'react-html-parser'

import Layout from '../components/layout'

const Projects = ({ location, data: { projects } }) => {
  return (
    <Layout location={location}>
      <div className="projects-page">
        <section className="banner_area">
          <div className="container">
            <div className="banner_inner_text">
              <h2>Portfolio</h2>
              <p>See our best works...</p>
            </div>
          </div>
        </section>
        <br/>
        {/* <div className="filter">
          <div className="active" data-filter="*"><a href="#">All</a></div>
          <div data-filter=".brand"><a href="#">Brand identity</a></div>
          <div data-filter=".design"><a href="#">Design</a></div>
          <div data-filter=".arc"><a href="#">Architecture</a></div>
          <div data-filter=".photo"><a href="#">Photography</a></div>
        </div> */}
        <br/>
        <div className="projects">
          {projects.nodes.map( ( { id, data }, i ) => (
            <div key={id} className={`project ${i % 2 && 'is-reversed'}`}>
              <div className="image">
                <Image fluid={data.image.localFile.childImageSharp.fluid} alt={data.image.alt} />
              </div>
              <div className="content">
                <h3>{data.title.text}</h3>
                <ul className="details">
                  <li><small>Client:</small> <span>{data.client.text}</span></li>
                  <li><small>Type:</small> <span>{data.type.text}</span></li>
                  <li><small>CMS:</small> <span>{data.cms_used.text}</span></li>
                  {data.built_with.text && (<li><small>Framework:</small> <span>{data.built_with.text}</span></li>)}
                </ul>
                {/* <div>{ReactHtmlParser(data.content.html)}</div> */}
                <br/>
                <div className="action">
                  <a href={data.url.url} target="_blank" rel="noopener noreferrer" className="more_btn">
                    View Project
                  </a>
                </div>
              </div>

            </div>
          ))}
        </div>
      </div>
    </Layout>
  )
}

export default Projects

export const query = graphql`query ProjectsPage {
    projects: allPrismicProject (sort: { fields: data___order, order: ASC }) {
      nodes {
        id
        data {
          title {
            text
          }
          url {
            url
          }
          content {
            html
          }
          client {
            text
          }
          cms_used {
            text
          }
          built_with {
            text
          }
          type {
            text
          }
          image {
            alt
            localFile {
              childImageSharp {
                fluid (maxHeight: 800, cropFocus: CENTER, quality: 85) {
                  ...GatsbyImageSharpFluid_withWebp_tracedSVG
                }
              }
            }
          }
        }
      }
    }
  }
`
