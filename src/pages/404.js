import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const NotFoundPage = ({ location }) => (
  <Layout location={location}>
    <div className="thanks-page">
      <h1>NOT FOUND</h1>
      <h4>You just hit a route that doesn&#39;t exist... the sadness.</h4>
      <small>Try the random route generator instead...</small>
      <Link
        className="button-footer-newsletter"
        style={{ width: '5rem' }}
        to="/blog"
      >
        View Blog
      </Link>
    </div>
  </Layout>
)

export default NotFoundPage
