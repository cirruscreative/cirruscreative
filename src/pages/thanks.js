import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const SecondPage = props => (
  <Layout location={props.location}>
    <div className="thanks-page">
      <h1>Thank you!</h1>
      <h4>
        We love new mail, so we will be opening that right about... now...
      </h4>
      <p>
        We'll get back to you as soon as we can. In the meantime, why not browse
        our blog?
      </p>
      <Link
        className="button-footer-newsletter"
        style={{ width: '5rem' }}
        to="/blog"
      >
        View Blog
      </Link>
    </div>
  </Layout>
)

export default SecondPage
