import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import {Helmet} from 'react-helmet'
import readingTime from 'reading-time'
import date from 'date-and-time'
import HtmlParser from 'react-html-parser'
// Import Components
import Layout from '../components/layout'
// Import Images

// Calculate time to read
const timeToRead = text => {
  const stats = readingTime(text)
  return stats.text
}

const datePublished = postDate => {
  const parsedDate = date.parse(postDate, 'YYYY MM DD')
  const day = date.format(parsedDate, 'DD')
  const monthYear = date.format(parsedDate, 'MMMM YYYY')
  return { day, monthYear }
}

const BlogPostTemplate = ({ data: { post }, location }) => (
  <Layout location={location}>
    <Helmet>
      <title>{post.data.title.text} - Cirrus Creative</title>
      <meta name="description" content={post.data.seo_description.text} />
    </Helmet>
    <div className="blog-post">
      <div className="blog-post-hero">
        <h1>Blog</h1>
        <h4>Read the news</h4>
        <br />
        <div className="hr" />
      </div>
      <div className="blog-post-content">
        <div className="post-header">
          <div className="post-header-image">
            <Img
              fixed={post.data.featured_image.localFile.childImageSharp.fixed}
            />
          </div>
          <div className="post-header-date">
            <span className="day">{datePublished(post.data.date).day}</span>{' '}
            <br />
            <span className="month-year">
              <small>{datePublished(post.data.date).monthYear}</small>
            </span>
          </div>
        </div>
        <div className="post-body">
          <h4 className="post-body-title">{post.data.title.text}</h4>
          <p className="post-body-author">
            <small>
              {post.data.author.document[0].data.name.text} | General
            </small>
          </p>
          <div className="post-body-excerpt">
            <small>{timeToRead(post.data.text.html)}</small>
          </div>
          <div className="post-body-content">
            <p>{HtmlParser(post.data.text.html)}</p>
          </div>
        </div>
      </div>
      <div className="blog-post-author">
        <div className="profile-image">
          <Img
            fixed={
              post.data.author.document[0].data.image.localFile.childImageSharp
                .fixed
            }
          />
        </div>
        <div className="profile">
          <h5 className="profile-title">
            {post.data.author.document[0].data.name.text} - Author
          </h5>
          <p className="profile-bio">
            {HtmlParser(post.data.author.document[0].data.bio.html)}
          </p>
        </div>
      </div>
    </div>
  </Layout>
)

export default BlogPostTemplate

export const query = graphql`
  query BlogPostTemplateQuery($slug: String!) {
    post: prismicBlogPost(uid: { eq: $slug }) {
      uid
      data {
        author {
          document {
            ... on PrismicAuthor {
              data {
                name {
                  text
                }
                bio {
                  html
                }
                image {
                  localFile {
                    childImageSharp {
                      fixed(height: 100, width: 100, cropFocus: ATTENTION) {
                        ...GatsbyImageSharpFixed_withWebp_tracedSVG
                      }
                    }
                  }
                }
              }
            }
          }
        }
        title {
          text
        }
        seo_description {
          text
        }
        text {
          html
        }
        date
        featured_image {
          localFile {
            childImageSharp {
              fixed(width: 1820, height: 400, cropFocus: ATTENTION) {
                ...GatsbyImageSharpFixed_withWebp_tracedSVG
              }
            }
          }
        }
      }
    }
  }
`
