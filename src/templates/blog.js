import React from 'react'
import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import { Helmet } from 'react-helmet'
import readingTime from 'reading-time'
import date from 'date-and-time'
// Import Components
import Layout from '../components/layout'
// Import Images

// Calculate time to read
const timeToRead = text => {
  const stats = readingTime(text)
  return stats.text
}

const datePublished = postDate => {
  const parsedDate = date.parse(postDate, 'YYYY MM DD')
  const day = date.format(parsedDate, 'DD')
  const monthYear = date.format(parsedDate, 'MMMM YYYY')
  return { day, monthYear }
}

const excerpt = text => {
  if (text.length < 500) return text
  return `${text.slice(0, 500)}...`
}

// class Quote extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       quote: null,
//       isLoading: true
//     }
//   }

//   componentDidMount () {
//     fetch('https://quotes.rest/qod')
//       .then(response => response.json())
//       .then(({ contents: { quotes } }) => this.setState({ quote: quotes[0] }) )
//       .then(() => this.setState({ isLoading: false }))
//   }

//   render () {
//     const { quote, isLoading } = this.state
//     if (isLoading) {
//       return <p>Loading ...</p>;
//     }
//     return (
//       <div className="blog-qod">
//         <p>{quote.quote}</p>
//         <small>{quote.author}</small>
//       </div>
//     )
//   }
// }

const BlogPage = ({ data: { blog }, location }) => (
  <Layout location={location}>
    <Helmet htmlAttributes={{ lang: 'en' }}>
      <meta charSet="utf-8" />
      <title>Blog - Cirrus Creative</title>
      <meta
        name="description"
        content="The Cirrus Creative blog, filled with informative and useful articles."
      />
    </Helmet>
    <div className="blog-page">
      <div className="blog-page-hero">
        <h1>Blog</h1>
        <h4>Read the news</h4>
        <br />
        <div className="hr" />
      </div>
      <div className="blog-page-list">
        {blog.edges.map(({ node: post }, index) => (
          <div className="post" key={index}>
            <div className="post-header">
              <div className="post-header-image">
                <Img
                  fixed={
                    post.data.featured_image.localFile.childImageSharp.fixed
                  }
                />
              </div>
              <div className="post-header-date">
                <span className="day">{datePublished(post.data.date).day}</span>{' '}
                <br />
                <span className="month-year">
                  <small>{datePublished(post.data.date).monthYear}</small>
                </span>
              </div>
            </div>
            <div className="post-body">
              <h4 className="post-body-title">{post.data.title.text}</h4>
              <p className="post-body-author">
                <small>
                  {post.data.author.document[0].data.name.text} | General
                </small>
              </p>
              <div className="post-body-excerpt">
                <small>{timeToRead(post.data.text.text)}</small>
                <p>{excerpt(post.data.text.text)}</p>
              </div>
              <div className="post-body-read-more">
                <Link
                  to={`/post/${post.uid}`}
                  className="button-footer-newsletter"
                >
                  Read more
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
      {/* <div className="blog-page-sidebar">
        <div className="sidebar-search-form">
          <input type="text"/>
        </div>
        <div className="sidebar-categories">
          <h5>Categories</h5>
        </div>
        <div className="sidebar-quote">
          <h5>Quote</h5>
          <Quote />
        </div>
      </div> */}
      {/* <div className="blog-page-pagination">
        <span>1 2 3 4 5</span>
      </div> */}
    </div>
  </Layout>
)

export default BlogPage

export const query = graphql`
  query BlogQuery {
    blog: allPrismicBlogPost(sort: { fields: data___date, order: DESC }) {
      edges {
        node {
          uid
          data {
            author {
              document {
                ... on PrismicAuthor {
                  data {
                    name {
                      text
                    }
                  }
                }
              }
            }
            title {
              text
            }
            text {
              text
            }
            date
            featured_image {
              localFile {
                childImageSharp {
                  fixed(width: 1820, height: 400, cropFocus: ATTENTION) {
                    ...GatsbyImageSharpFixed_withWebp_tracedSVG
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
