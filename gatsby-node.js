// const path = require(`path`)

// exports.createPages = ({ graphql, actions }) => {
//   const { createPage } = actions
//   return new Promise((resolve, reject) => {
//     graphql(`
//       {
//         allPrismicBlogPost {
//           edges {
//             node {
//               uid
//             }
//           }
//         }
//       }
//     `).then(result => {
//       result.data.allPrismicBlogPost.edges.forEach(({ node }) => {
//         createPage({
//           path: `post/${node.uid}`,
//           component: path.resolve(`./src/templates/blog-post.js`),
//           context: {
//             // Data passed to context is available
//             // in page queries as GraphQL variables.
//             slug: node.uid,
//           },
//         })
//       })
//       resolve()
//     })
//   })
// }
